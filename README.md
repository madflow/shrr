# Overview

> Share files via HTTP

* Share a file or directory with any computer in the same network over HTTP
* Change the exposed IP address and port

# Usage

```bash

Usage: shrr [OPTIONS] [PATH]

  Shrr (Sharrrrerrrrr)

Options:
  --port INTEGER
  --address TEXT
  --uptime INTEGER  Terminate server after <uptime> seconds
  --help            Show this message and exit.


# Example:

shrr --port 3333 --address localhost /home    
```

# Installation

## Pip

Use the custom pypi index located here: https://madflow.gitlab.io/shrr

```bash
pip install flask click netifaces colorama
pip install --index-url=https://madflow.gitlab.io/shrr shrr
```

## Arch Linux

```bash
yaourt -S shrr
```

## Docker

```bash
docker run -v $PWD:/app -p 5000:5000 --rm madflow/shrr
```

## From Source

```bash
git clone https://gitlab.com/madflow/shrr.git

pip install -r requirements.txt
python setup.py yarn install --dev --emoji
python setup.py yarn run build:prod 
```

# Testing

```bash
virtualenv venv
. venv/bin/activate
pip install -r requirements.txt
pip install -r requirements.dev.txt

python setup.py yarn install --dev --emoji
python setup.py yarn run build:prod 
```

**Running the test suite***

```bash
python setup.py test
python setup.py yarn run tools:webriver-update
python setup.py yarn run build:prod 
python setup.py yarn run test:e2e
```

# Resources (Later to be removed)

* Serve the current directory with docker and port 5000
```docker run -v $PWD:/app -p 5000:5000 -rm madflow/shrr```
* https://packaging.python.org/tutorials/distributing-packages/
* https://github.com/djblets/djblets (Setup with node JS)
* https://docs.gitlab.com/ce/ci/yaml/README.html
* http://click.pocoo.org/6/setuptools/
* https://github.com/micahflee/onionshare
* https://github.com/pypa/sampleproject
* virtualenv --python=python3.6 ven
*  python setup.py sdist bdist_wheel
* https://wiki.archlinux.org/index.php/Python_package_guidelines
* https://stackoverflow.com/questions/677577/distutils-how-to-pass-a-user-defined-parameter-to-setup-py
* https://stackoverflow.com/questions/1710839/custom-distutils-commands
