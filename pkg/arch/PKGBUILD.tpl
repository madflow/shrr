# Do not edit PKGBUILD directly. Edit PKGBUILD.tpl and update the setup.json file

# Maintainer: {{author}} <{{author_email}}>
pkgname={{name}}
pkgver={{version}}
pkgrel=1
pkgdesc="{{description}}"
arch=('any')
url="{{url}}"
license=({{license}})
depends=('python>=3.3' 'python-flask' 'python-netifaces' 'python-colorama' 'python-qrcode')
makedepends=('python-setuptools' 'python-pip' 'nodejs' 'yarn')
source=({{source}})
md5sums=({{md5sums}})

build() {
  cd "$srcdir/$pkgname-{{commit_id}}-{{commit_id}}"
  python setup.py yarn install --dev --emoji
  python setup.py yarn run build:prod
}

package() {
  cd "$srcdir/$pkgname-{{commit_id}}-{{commit_id}}"
  python setup.py install --root="$pkgdir/" --optimize=1
}