#!/bin/bash
# https://github.com/postgres/pgadmin4/blob/master/pkg/pip/build.sh

# Runtime checks
if [ ! -d tests -a ! -d shrr ]; then
    echo This script must be run from the top-level directory of the source tree.
    exit 1
fi

if [ ! -d .git -a ! -f .git/config ]; then
    echo This script must be run from a git checkout of the source tree.
    exit 1
fi

BASE_PATH=`pwd`
echo "Starting from $BASE_PATH"

echo "Install Javascript dependencies and run Webpack build"
cd shrr
yarn install --dev
yarn run build:prod
cd $BASE_PATH

echo "Building wheel"
python setup.py bdist_wheel

echo "Building sdist"
python setup.py sdist


