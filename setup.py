# pylint: disable=C0103, C0111
"""Setup"""

import hashlib
from os import path, environ, chdir, unlink
import sys
import requests
import shutil
from subprocess import check_output
import tempfile
from setuptools import setup, find_packages, Command
from shrr.util import SetupValues, SimpleTemplate

base_path = path.abspath(path.dirname(__file__))
app_path = path.join(base_path, 'shrr')
setup_values = SetupValues(path.join(base_path, 'setup.json'))


class SimpleCommand(Command):
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass


class YarnCommand(SimpleCommand):

    command_consumes_arguments = True

    def initialize_options(self):
        self.args = None

    def run(self):

        yarn_args = []
        for arg in sys.argv[::-1]:
            yarn_args.append(arg)
            if arg == 'yarn':
                break

        yarn_args.reverse()
        chdir(app_path)
        check_output(yarn_args)
        chdir(base_path)


class PkgArchCommand(Command):

    description = 'Generate a PKGBUILD from a template'

    user_options = [
        ('commit-id=', None, 'The commit id')
    ]

    def initialize_options(self):
        self.commit_id = ''

    def finalize_options(self):
        pass

    def run(self):

        template = SimpleTemplate(
            path.join(base_path, 'pkg', 'arch'), 'PKGBUILD.tpl')
        output = path.join(base_path, 'pkg', 'arch', 'PKGBUILD')

        source_url = 'https://gitlab.com/madflow/shrr/repository/%s/archive.tar.gz' % (self.commit_id)
        response = requests.get(source_url, stream=True)

        source = self.commit_id.split("/")[-1]
        with open(source, 'wb') as out_file:
            shutil.copyfileobj(response.raw, out_file)
        del response
        hashwheel = hashlib.md5(open(source, 'rb').read()).hexdigest()
        unlink(source)

        values = setup_values.get_all()
        values['source'] = source_url
        values['md5sums'] = hashwheel
        values['commit_id'] = self.commit_id
        template.replace_and_write(values, output)
        chdir(path.join(base_path, 'pkg', 'arch'))
        chdir(base_path)


class GitlabTagCommand(SimpleCommand):

    endpoint = 'https://gitlab.com/api/v4/projects/%s/repository/tags'

    def run(self):

        if environ.get('CI', None) is None:
            print('Not in CI context')
            return

        url = self.endpoint % environ.get('CI_PROJECT_ID')
        headers = {'PRIVATE-TOKEN': environ.get('PRIVATE_TOKEN')}
        commit_sha = environ.get('CI_COMMIT_SHA')
        commit_ref_name = environ.get('CI_COMMIT_REF_NAME')
        tag_name = '%s-%s' % (commit_ref_name, commit_sha[:8])
        message = 'Automated release'
        release_description = 'Release in %s with %s' % (
            commit_ref_name, commit_sha[:8])
        payload = (('tag_name', tag_name), ('ref', commit_sha), ('message',
                                                                 message), ('release_description', release_description))
        r = requests.post(url=url, headers=headers, data=payload)
        if r.status_code != 201:
            raise ValueError(
                'Status code 201 expected. Got "%s"' % r.status_code)


# Get the long description from the README file
with open(path.join(base_path, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name=setup_values.get('name'),
    author=setup_values.get('author'),
    author_email=setup_values.get('author_email'),
    url=setup_values.get('url'),
    description=setup_values.get('description'),
    long_description=long_description,
    license=setup_values.get('license'),
    keywords=setup_values.get('keywords'),
    packages=find_packages(exclude=['node_modules'], include=['shrr']),
    include_package_data=True,
    zip_safe=False,
    version=setup_values.get('version'),
    install_requires=[
        'flask', 'click', 'netifaces', 'colorama', 'qrcode'
    ],
    setup_requires=[
        'pytest-runner',
    ],
    tests_require=[
        'pytest',
    ],
    entry_points={
        'console_scripts': [
            'shrr=shrr.cli:main',
        ],
    },
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 3 - Alpha',

        # Pick your license as you wish (should match "license" above)
        'License :: OSI Approved :: MIT License',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
    cmdclass={
        'pkg_arch': PkgArchCommand,
        'gitlab_tag': GitlabTagCommand,
        'yarn': YarnCommand,
    },
)
