# pylint: disable=C0103

"""App"""

import io
import os
from pathlib import Path

from netifaces import interfaces, ifaddresses, AF_INET

from flask import Flask, abort, render_template, send_from_directory, send_file, make_response
import qrcode
import qrcode.image.svg

app = Flask(__name__)


def scan_path(path):
    """Read files and folders in a directory"""
    files = []
    path = Path(path)
    for x in sorted(path.iterdir()):
        files.append(x)

    return files


@app.errorhandler(404)
def page_not_found(error):
    """404"""
    return render_template('404.html'), 404


@app.errorhandler(403)
def page_forbidden(error):
    """403"""
    return render_template('403.html'), 403


@app.route("/")
def index():
    """Index"""
    base = app.config['BASE_PATH'] if 'BASE_PATH' in app.config else Path(os.getcwd())
    files = scan_path(base)
    return render_template('index.html', current=base, base=base, files=files, breadcrumbs=[])


@app.route("/qr")
def qr():
    """QR Code"""
    base_url = 'http://%s:%s' % (app.config['ADDRESS'], app.config['PORT'])

    qr = qrcode.make(base_url, image_factory=qrcode.image.svg.SvgImage)
    stream = io.BytesIO()
    qr.save(stream)
    response = make_response(stream.getvalue())
    response.headers['Content-Type'] = 'image/svg+xml'
    return response


# @app.route('/', defaults={'path': ''})
@app.route("/<path:path>")
def inspect(path):
    """Inspect"""
    base = app.config['BASE_PATH'] if 'BASE_PATH' in app.config else Path(os.getcwd())
    new_path = os.path.join(base, path)
    current = Path(new_path)

    if current.is_dir() is False and current.is_file() is False:
        return abort(404)

    if current.is_file():
        return send_from_directory(current.parent, current.name, as_attachment=True)
    try:
        files = scan_path(new_path)
    except PermissionError:
        return abort(403)

    for ifaceName in interfaces():
        ifaces = [i['addr'] for i in ifaddresses(ifaceName).setdefault(AF_INET, [{'addr': ''}])]
        # print('%s: %s' % (ifaceName, ', '.join(addresses)))
        addresses = (ifaceName, ifaces)

    breadcrumbs = []
    for parent in current.parents:
        breadcrumbs.append(parent)
        if parent == base:
            break
    return render_template(
        'index.html',
        base=base,
        current=current,
        files=files,
        addresses=addresses,
        breadcrumbs=breadcrumbs
    )
