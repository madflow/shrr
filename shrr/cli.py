# pylint: disable=C0103

import os
from pathlib import Path
from multiprocessing import Process
from time import sleep

import click
from . import app


@click.command()
@click.argument('path', type=click.Path(exists=True), required=False)
@click.option('--port', default=5000, type=int)
@click.option('--address', default='0.0.0.0')
@click.option('--uptime', default=None, type=int, help='Terminate server after <uptime> seconds')
def main(path=None, port=5000, address='0.0.0.0', uptime=None):

    """Shrr (Sharrrrerrrrr)"""
    if path is '.' or path is None:
        target_path = Path(os.getcwd())
    else:
        target_path = Path(path)

    debug = os.getenv('SHRR_DEBUG')
    app.config['BASE_PATH'] = target_path
    app.config['ADDRESS'] = address
    app.config['PORT'] = port

    base_url = 'http://%s:%s' % (address, port)

    click.secho('-' * 50, bold=True)
    click.secho('Serving: %s' % (base_url),  fg='green')
    click.secho('QR Code: %s/qr' % (base_url),  fg='green')
    click.secho('-' * 50, bold=True)

    if debug:
        app.run(address, port, True)
    else:
        server = Process(target=app.run, args=(address, port, debug))
        if uptime is not None:
            click.secho('The server will shut down after %d seconds' % uptime, blink=True, bold=True)
            server.start()
            sleep(uptime)
            click.secho('Shutting down...', blink=True, bold=True)
            server.terminate()
        else:
            server.start()

        server.join()
