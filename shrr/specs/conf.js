exports.config = {
  capabilities: {
    browserName: "chrome",
    chromeOptions: {
      args: ["no-sandbox"]
    }
  },
  directConnect: true,
  specs: ["smoke-spec.js"]
};
