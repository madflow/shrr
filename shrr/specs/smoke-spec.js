describe('e2e fixtures', function() {
  it('should contain a file called test.xyz', function() {
    browser.waitForAngularEnabled(false);
    browser.get('http://localhost:3333');
    var xyz = element(by.cssContainingText('.file-name', 'test.xyz'));
    expect(xyz.getText()).toEqual('test.xyz');
  });
});

