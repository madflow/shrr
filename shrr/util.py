import json
from os import path
from jinja2 import Environment, FileSystemLoader, select_autoescape


class SetupValues(object):
    config_file = None

    def __init__(self, file):
        self.config_file = file

    def get_all(self):
        with open(self.config_file) as json_data:
            data = json.load(json_data)
            return data

    def get(self, key):
        data = self.get_all()
        return data[key]


class SimpleTemplate(object):
    template = None
    search_path = None
    jinja_env = None

    def __init__(self, search_path, template):
        self.search_path = search_path
        self.template = template
        self.jinja_env = Environment(
            loader=FileSystemLoader(self.search_path),
            autoescape=select_autoescape(['html', 'xml', 'json'])
        )

    def replace_and_write(self, dict, file_path):
        template = self.jinja_env.get_template(self.template)
        content = template.render(dict)
        with open(file_path, "w") as text_file:
            text_file.write(content)
