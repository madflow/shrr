# pylint: disable=W0621,W0613
"""Tests"""

import pytest

from shrr import app

@pytest.fixture
def client(request):
    """Client fixture"""
    app.config['TESTING'] = True
    app.config['ADDRESS'] = '0.0.0.0'
    app.config['PORT'] = 9119
    return app.test_client()

def test_index(client):
    """Index page yields a 200"""
    response = client.get('/')
    assert response.status_code == 200

def test_qr(client):
    """Index page yields a 200"""
    response = client.get('/qr')
    assert response.status_code == 200
    assert response.content_type == 'image/svg+xml'
