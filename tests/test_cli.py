from click.testing import CliRunner

from shrr import main

from time import sleep

def test_no_args_and_uptime():
    runner = CliRunner()
    with runner.isolated_filesystem():
        result = runner.invoke(main, args=['--uptime', '1'])
        assert result.exit_code == 0

def test_invalid_dir():
    runner = CliRunner()
    with runner.isolated_filesystem():
        result = runner.invoke(main, args=['noo'])
        assert 'Error: Invalid value for "path": Path "noo" does not exist.' in result.output

def test_invalid_port():
    runner = CliRunner()
    with runner.isolated_filesystem():
        result = runner.invoke(main, args=['--port', 'nope'])
        assert 'Invalid value for "--port": nope is not a valid integer' in result.output

def test_invalid_address():
    runner = CliRunner()
    with runner.isolated_filesystem():
        result = runner.invoke(main, args=['--address', 'njet'], catch_exceptions=True)

def test_qr_in_console_ouput():
    runner = CliRunner()
    with runner.isolated_filesystem():
        result = runner.invoke(main, args=['--uptime', '3', '--port', 6543])
        assert ':6543' in result.output